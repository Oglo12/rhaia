use clap::{Parser, Subcommand};

#[derive(Parser)]
/// Rhaia - Create/run Rhai projects easily.
pub struct Cli {
    #[clap(subcommand)]
    pub command: Option<Command>,
}

#[derive(Subcommand)]
pub enum Command {
    /// Run project
    Run,

    /// Create new Rhai project
    New { name: String },
}
