mod cli;

use std::io;
use clap::Parser;
use piglog::prelude::*;
use rustyline::DefaultEditor;
use rustyline::error::ReadlineError;
use colored::Colorize;
use fspp::*;
use serde::{Serialize, Deserialize};

const VERSION: &str = clap::crate_version!();

enum ExitCode {
    Success,
    Fail,
}

#[derive(Serialize, Deserialize, Default)]
#[serde(deny_unknown_fields, default)]
struct Config {
    name: String,
    version: String,
    authors: Vec<String>,
    rhaia_version: String,
}

impl Config {
    fn default() -> Self {
        return Self {
            name: "new-rhai-project".into(),
            version: "0.1.0".into(),
            authors: Vec::new(),
            rhaia_version: VERSION.into(),
        };
    }
}

fn main() {
    if let ExitCode::Fail = app() {
        std::process::exit(1);
    }
}

fn app() -> ExitCode {
    let args = cli::Cli::parse();

    match args.command {
        Some(s) => {
            match s {
                cli::Command::New { name } => match new(name.as_str()) {
                    Ok(_) => (),
                    Err(e) => {
                        piglog::fatal!("Failed to create new project! (Error: {e:?})");

                        return ExitCode::Fail;
                    },
                },
                cli::Command::Run => match run() {
                    Ok(_) => (),
                    Err(e) => {
                        piglog::fatal!("Failed to run project! (Error: {e:?})");

                        return ExitCode::Fail;
                    },
                },
            };
        },
        None => repl(),
    };

    return ExitCode::Success;
}

fn repl() {
    let left = "[".bright_black().bold();
    let right = "]".bright_black().bold();
    let title = "Rhai".bright_yellow().bold();
    let prompt = ">>>".bright_green().bold();

    let mut editor = DefaultEditor::new().unwrap();

    let engine = rhai::Engine::new();
    let mut scope = rhai::Scope::new();

    loop {
        let code = match editor.readline(&format!("{left}{title}{right} {prompt} ")) {
            Ok(o) => o,
            Err(e) => {
                match e {
                    ReadlineError::Eof => return,
                    ReadlineError::Interrupted => { "".to_string() },
                    _ => "".to_string(),
                }
            },
        };

        editor.add_history_entry(&code).unwrap();

        let _: () = match engine.eval_with_scope(&mut scope, &code) {
            Ok(o) => o,
            Err(e) => {
                println!("{}", format!("{e:#?}").bright_red().bold());
            },
        };
    }
}

fn run() -> Result<(), io::Error> {
    if Path::new("rhaia.toml").exists() == false {
        piglog::fatal!("Couldn't find 'rhaia.toml' in the current directory!");

        std::process::exit(1);
    }

    std::env::set_current_dir("src")?;

    let engine = rhai::Engine::new();

    let _: () = match engine.eval_file("main.rhai".into()) {
        Ok(o) => o,
        Err(e) => {
            println!("{}", format!("{e:#?}").bright_red().bold());

            std::process::exit(1);
        },
    };

    return Ok(());
}

fn new(name: &str) -> Result<(), io::Error> {
    let path = Path::new(name);
    let src = path.add_str("src");

    let mut config = Config::default();
    config.name = name.into();

    directory::create(&path)?;
    directory::create(&src)?;

    file::write("print(\"Hello, world!\");\n", &src.add_str("main.rhai"))?;
    file::write(&toml::to_string(&config).unwrap(), &path.add_str("rhaia.toml"))?;

    piglog::success!("Created new project: {}", path.to_string());

    return Ok(());
}
