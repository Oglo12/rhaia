# Rhaia - Create/Run Rhai Projects

Rhaia is a tool to run and create projects for the Rhai programming language.
You can also just use the Rhaia REPL.



# Usage:

REPL: `rhaia`

New Project: `rhaia new <PROJECT NAME>`

Run Project: `rhaia run`

Help: `rhaia --help`
